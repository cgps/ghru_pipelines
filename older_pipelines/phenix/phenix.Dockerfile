FROM ubuntu:16.04
# File Author / Maintainer
MAINTAINER Anthony Underwood

# Update the repository sources list
RUN apt-get update
RUN apt-get -y install software-properties-common python-software-properties
RUN add-apt-repository ppa:openjdk-r/ppa
RUN apt-get update
# install java
RUN apt-get -y install openjdk-7-jre
# install other files necessary for running commands
RUN apt-get -y install zip wget libfindbin-libs-perl git
# get and fastqc
RUN wget http://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v0.11.5.zip
RUN unzip fastqc_v0.11.5.zip
RUN chmod 755 FastQC/fastqc
RUN mv FastQC /usr/local/
RUN ln -s /usr/local/FastQC/fastqc /usr/local/bin/fastqc
# install trimmomatic
RUN wget http://www.usadellab.org/cms/uploads/supplementary/Trimmomatic/Trimmomatic-0.36.zip
RUN unzip Trimmomatic-0.36.zip
RUN mv Trimmomatic-0.36 /usr/local/trimmomatic
# install python
RUN apt-get -y install python python-pip
RUN pip install scipy numpy

# install make tools and then download and compile bwa
RUN apt-get -y install make gcc zlib1g-dev libncurses5-dev libncursesw5-dev libbz2-dev liblzma-dev
RUN wget https://downloads.sourceforge.net/project/bio-bwa/bwa-0.7.15.tar.bz2
RUN tar xvfj bwa-0.7.15.tar.bz2
RUN cd bwa-0.7.15; make
RUN mv  bwa-0.7.15/bwa /usr/local/bin/

# Make GATK available
COPY GenomeAnalysisTK.jar /usr/local/bin/
RUN chmod 644 usr/local/bin/GenomeAnalysisTK.jar
ENV GATK_JAR=/usr/local/bin/GenomeAnalysisTK.jar

# download Picard
RUN wget https://github.com/broadinstitute/picard/releases/download/1.141/picard-tools-1.141.zip
RUN unzip picard-tools-1.141.zip
RUN mv picard-tools-1.141/picard.jar /usr/local/bin/
ENV PICARD_JAR=/usr/local/bin/picard.jar

# Download PHEnix Python dependencies
# python libraries, intsalling cython before PyVCF
RUN pip install cython
RUN pip install bintrees==2.0.2 biopython==1.65 coverage==4.0.3 nose==1.3.7 PyVCF==0.6.8 PyYAML==3.11 Sphinx==1.3.5 sphinx-argparse==0.1.15 sphinx-rtd-theme==0.1.9 numpydoc==0.6.0
# samtools
RUN wget https://github.com/samtools/samtools/releases/download/1.4/samtools-1.4.tar.bz2
RUN tar xvfj samtools-1.4.tar.bz2
RUN cd samtools-1.4; make; make prefix=/usr/local install
# bcf tools
RUN wget https://github.com/samtools/bcftools/releases/download/1.4/bcftools-1.4.tar.bz2
RUN tar xvfj bcftools-1.4.tar.bz2
RUN cd bcftools-1.4; make; make prefix=/usr/local install

# Install PHEnix
RUN git clone https://github.com/phe-bioinformatics/PHEnix.git
RUN pip install -e PHEnix

# Install enaBrowserTools
RUN git clone https://github.com/enasequence/enaBrowserTools.git

# Install SRA toolkit
RUN wget https://ftp-trace.ncbi.nlm.nih.gov/sra/sdk/2.9.2/sratoolkit.2.9.2-ubuntu64.tar.gz
RUN tar xvfz sratoolkit.2.9.2-ubuntu64.tar.gz

# Install aspera
RUN mkdir /aspera/
RUN wget https://download.asperasoft.com/download/sw/connect/3.8.1/ibm-aspera-connect-3.8.1.161274-linux-g2.12-64.tar.gz -P /aspera/
RUN tar -xvzf /aspera/ibm-aspera-connect-3.8.1.161274-linux-g2.12-64.tar.gz -C /aspera/
RUN useradd -m aspera
RUN usermod -L aspera
RUN runuser -l aspera -c '/aspera/ibm-aspera-connect-3.8.1.161274-linux-g2.12-64.sh'
ADD aspera.ini /aspera.ini

# Install seqtk
RUN apt-get install -y zlib1g-dev
RUN git clone https://github.com/lh3/seqtk.git
RUN cd seqtk; make; mv seqtk /usr/local/bin/

# Install mash
RUN wget https://github.com/marbl/Mash/releases/download/v2.0/mash-Linux64-v2.0.tar
RUN tar xf mash-Linux64-v2.0.tar; mv mash-Linux64-v2.0/mash /usr/local/bin/

# INstall IQ-TREE
RUN apt-get install -y iqtree

# Clean up APT when done.
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Update path
ENV PATH "/enaBrowserTools/python3:$PATH"
