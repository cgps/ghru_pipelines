# PHEnix workflow
This [Nextflow](https://www.nextflow.io/) workflow can be used to process short read fastq files through the SNP calling pipeline wrapped into the PHEnix software.

Typically the workflow should be run as follows

```
nextflow run phenix.nf [options] -with-docker bioinformant/ghru-phenix:1.0 -resume 
```

To run the test sets either of the following commands will work
 - Using both local fastqs and a local database as input
   ```
   nextflow run phenix.nf  --input_dir test_in --fastq_pattern '*_{1,2}.fastq.gz'  --output_dir test_output --reference salmonella_AM933172.fasta --snp_pipeline_config snp_pipeline_config.yml -with-docker bioinformant/ghru-phenix:1.0 -resume
   ```
 - Fetching fastqs using a list of accession numbers
   ```
   nextflow run phenix.nf  --accession_number_file accessions.txt  --output_dir test_output --reference salmonella_AM933172.fasta --snp_pipeline_config snp_pipeline_config.yml -with-docker bioinformant/ghru-phenix:1.0 -resume
   ```

## Workflow arguments
The mandatory options that should be supplied are
  - A source of the fastq files specified as either of the following
    - local files on disk using the `--input_dir` and `--fastq_pattern` arguments
    - from a list of short read archive ERR or SRR accession numbers contained within a file specified by the `--accession_number_file` argument
  - The output from the pipeline will be written to the directory specified by the `--output_dir` argument
  - A close reference sequence to which the sequences will be mapped should be supplied as a file in fasta format using the `--reference` argument
  - The path to a yml file containing the SNP filtering options as described in the PHEnix documentation [here](https://phenix.readthedocs.io/en/latest/Calling%20SNPs.html#config) and [here](https://phenix.readthedocs.io/en/latest/Filters.html). An example of the file contents can be found in the snp_pipeline_config.yml file in this repo, the contents of which are as follows:
    ```
    mapper: bwa
    mapper-options: -t 1
    variant: gatk
    variant-options: --sample_ploidy 2 --genotype_likelihoods_model BOTH -rf BadCigar -out_mode EMIT_ALL_SITES -nt 1

    filters:
    ad_ratio: 0.9
    min_depth: 10
    qual_score: 40
    mq_score: 30
    mq0_ratio: 0.1

    annotators:
      - coverage
    ```
Optional arguments include

  - The `--depth_cutoff` argument will downsample each sample to an approximate depth of the value supplied e.g 50 means downsample to 50x depth of coverage. If not specified no downsampling will occur.
  - `--tree` If specified a maximum likelihood tree will be created using IQ-TREE
  - `--max_fraction_of_column_Ns` The maximum fraction of Ns in any column within the SNP alignment (default 0.5)
  - `--max_fraction_of_column_gaps` The maximum fraction of gaps in any column within the SNP alignment (default 0.5)

## Workflow process
The workflow consists of the following steps

1. (Optional) Fetch reads from the ENA
2. Trim reads using trimmomatic (dynamic MIN_LEN based on 30% of the read length)
3. Map reads to reference
4. Call variants using GATK version 2.7.4
   **N.B** The [licence](https://software.broadinstitute.org/gatk/download/archive) for GATK states

    "These packages are made available for free to academic researchers under a limited license for non-commercial use." If you can not fulfil these conditions please **do not** use this software.
5. Filters variants based on the arguments specified in the snp_pipeline_config.yml config file
6. Produce a fasta file from the filtered VCFs


A sumary of this process is shown below in the diagram that was generated when running Nextflow using the -with-dag command

![workflow diagram](dag.png)

## Workflow outputs
Within the directory specified using the `--output_dir` argument the following outputs will be written:
 - (Optional) If accession numbers were used as the input source a directory called `fastqs` will contain the fastq file pairs for each accession number
 - A directory name `trimmed_fastqs` containing the fastqs after trimming
 - A directory named `filtered_vcfs` containing the VCF files that have been filtered for high quality SNPs
 - A fasta alignment of the SNPs found in each file named `snp_alignment.fasta`
 - (Optional) If you specified the --tree option then a newick tree `snp_alignment.fasta.treefile` and consensus tree `snp_alignment.fasta.contree` with bootstrap values will be generated using the ML algorithm as implemented in IQ-TREE


## Software used within the workflow
  - [PHEnix](https://phenix.readthedocs.io/en/latest/) Public Health England SNP calling pipeline.
  - [enaBrowserTools](https://github.com/enasequence/enaBrowserTools) A collection of scripts to assist in the retrieval of data from the ENA Browser.
  - [Trimmomatic](http://www.usadellab.org/cms/?page=trimmomatic) A flexible read trimming tool for Illumina NGS data.
  - [mash](https://mash.readthedocs.io/en/latest/) Fast genome and metagenome distance estimation using MinHash.
  - [seqtk](https://github.com/lh3/seqtk) A fast and lightweight tool for processing sequences in the FASTA or FASTQ format.
  - [IQ-TREE](http://www.iqtree.org/) Efficient software for phylogenomic inference
