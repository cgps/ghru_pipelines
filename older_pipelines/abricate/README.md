# Abricate workflow
This [Nextflow](https://www.nextflow.io/) workflow is designed to process multiple assembly files to predict AMR determinants or virulence factors using the [Abricate](https://github.com/tseemann/abricate) software. 

Typically the workflow should be run as follows

```
nextflow run assembly.nf [options] -with-docker bioinformant/ghru-abricate:1.0 -resume 
```

To run the test sets 
 - Using an updated ncbi database

    ```
   nextflow run abricate.nf --input_dir test_input --fasta_pattern '*_scaffolds.fasta' --output_dir test_output --database ncbi --update_database -with-docker bioinformant/ghru-abricate:1.0 -resume
   ```

The mandatory options that should be supplied are
  - A source of the fasta files specified as a combination of
    - `--input_dir` and 
    - `--fasta_pattern` e.g '*_scaffolds.fasta'
  - The output from the pipeline will be written to the directory specified by the `--output_dir` argument

Optional arguments include
  - `--database`: Options are plasmidfinder, ecoh, argannot, ncbi, ecoli_vf, vfdb, resfinder, card, virulencefinder. If not specified the default is resfinder.
  -  `--update_database`: Force update of database to latest version available

## Workflow process
The workflow consists of the following steps

1. Run abricate using each fasta file as an input with the database specified.
2. Summarise all abricate outputs in a single file called abricate_summary_<DATABASE>.tsv where <DATABASE> is that specified in the command

## Workflow outputs
These will be found in the directory specified by the `--output_dir` argument
 - A tabulated ouput for each sample name `<SAMPLE NAME>.tab`
 - A summary tsv file named `abricate_summary_<DATBASE NAME>.tsv`
## Software used within the workflow
  - [Abricate](https://github.com/tseemann/abricate)  Mass screening of contigs for antimicrobial and virulence genes
  