#!/usr/bin/env nextflow
/*

========================================================================================
                          Abricate AMR prediction pipeline
========================================================================================
 Abricate pipeline. Thanks to Torsten Seemann @torstenseemann for Abricate software: https://github.com/tseemann/abricate
 #### Authors
 Anthony Underwood @bioinformant <au3@sanger.ac.uk>
 Thanks to 
----------------------------------------------------------------------------------------
*/

// Pipeline version
version = "1.0"

def versionMessage(){
  log.info"""
    =========================================
     Abricate Pipeline version ${version}
    =========================================
  """.stripIndent()
}
def helpMessage() {
    log.info"""
    Usage:
    The typical command for running the pipeline is as follows:
    nextflow run -with-docker [options] abricate.nf
    Mandatory arguments:
      --input_dir        Path to input dir "must be surrounded by quotes"
      --output_dir       Path to output dir "must be surrounded by quotes"
      --fasta_pattern    The regular expression that will match fasta files e.g "*_scaffolds.fasta"
    Optional arguments:
      --database         Default is resfinder, options are plasmidfinder, ecoh, argannot, ncbi, ecoli_vf, vfdb, resfinder, card, virulencefinder
      --update_database Force update of database to latest version available
   """.stripIndent()
}

//  print help if required
params.help = false
// Show help message
if (params.help){
    versionMessage()
    helpMessage()
    exit 0
}

// Show version number
params.version = false
if (params.version){
    versionMessage()
    exit 0
}

/***************** Setup inputs and channels ************************/
// Defaults for configurable variables
params.input_dir = false
params.output_dir = false
params.fasta_pattern = false
params.database = false
params.update_database = false

// set up input_dir
input_dir = Helper.check_mandatory_parameter(params, "input_dir") - ~/\/$/
// set up output directory
output_dir = Helper.check_mandatory_parameter(params, "output_dir") - ~/\/$/
//  check a pattern has been specified and setup fasta file path
fasta_pattern = Helper.check_mandatory_parameter(params, "fasta_pattern")
fasta_filepath = input_dir + "/" + fasta_pattern

// setup database
if (params.database) {
  database =  Helper.check_parameter_value("database", params.database, ["plasmidfinder", "ecoh", "argannot", "ncbi", "ecoli_vf", "vfdb", "resfinder", "card", "virulencefinder"])
} else {
  database = "resfinder"
}

if (params.update_database) {

  process update_database{
    tag { "update database" }

    input:
    database

    output:
    file("sequences") into database_sequences

    script:
    """
    abricate-get_db --db ${database} --force
    cp /NGStools/miniconda/db/${database}/sequences sequences
    """
  }

} else {
  process get_database_sequences {
    tag { "get database" }

    input:
    database

    output:
    file("sequences") into database_sequences

    script:
    """
    cp /NGStools/miniconda/db/${database}/sequences sequences
    """
  }
}

Channel
    .fromPath(fasta_filepath)
    .ifEmpty { error "Cannot find any reads matching: ${fasta_filepath}" }
    .map{ file -> tuple(Channel.readPrefix(file, fasta_pattern), file) }
    .set { fasta_files }

// run abricate
process run_abricate{
  tag { sample_id }
  
  publishDir "${output_dir}",
    mode: "copy"

  input:
  file("sequences") from database_sequences
  set sample_id, file(fasta_file) from fasta_files

  output:
  file("${sample_id}.tab") into abricate_reports

  """
  mkdir -p abricate_databases/database_for_nextflow
  ln -s \$PWD/sequences abricate_databases/database_for_nextflow/sequences
  abricate --datadir abricate_databases --setupdb
  abricate --datadir abricate_databases  --db database_for_nextflow ${fasta_file} > ${sample_id}.tab
  """
}

// combine results
process abricate_summary {
  tag { "abricate_summary" }
  
  publishDir "${output_dir}",
    mode: "copy"

  input:
  database
  file(contig_files) from abricate_reports.collect { it }

  output:
  file("abricate_summary_${database}.tsv")

  """
  abricate --summary ${contig_files} > abricate_summary_${database}.tsv
  """
}

workflow.onComplete {
  Helper.complete_message(params, workflow, version)
}

workflow.onError {
  Helper.error_message(workflow)
}