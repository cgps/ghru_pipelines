#!/usr/bin/env nextflow
/*

========================================================================================
                          Ariba AMR prediction Pipeline
========================================================================================
 Predicting AMR determinants using the Ariba software: https://github.com/sanger-pathogens/ariba
 #### Authors
 Martin Hunt @martibartfast
 Anthony Underwood @bioinformant <au3@sanger.ac.uk>
----------------------------------------------------------------------------------------
*/

// Pipeline version
version = '1.0'

def versionMessage(){
  log.info"""
    =========================================
     Ariba Pipeline version ${version}
    =========================================
  """.stripIndent()
}
def helpMessage() {
    log.info"""
    Usage:
    The typical command for running the pipeline is as follows:
    nextflow run ariba.nf --input_dir <INPUT DIR> --output_dir <OUTPUT DIR> [options]
    Mandatory arguments:
      --input_dir      Path to input dir "must be surrounded by quotes"
      --output_dir     Path to output dir "must be surrounded by quotes"
    Options:
    One of these patterns to match input files must be specified
      --bam_pattern    The regular expression that will match bam files e.g '*.bam'
      --fastq_pattern  The regular expression that will match fastq files e.g '*_{1,2}.fastq.gz'
    One of these arguments to specify how the AMR database can be found/created
      --database_dir   Path to dir containing ariba resitance database "must be surrounded by quotes"
      --get_database   Specify a valid database from argannot, card, megares, plasmidfinder, resfinder, srst2_argannot, vfdb_core, vfdb_full, virulencefinder

    Optional arguments:
      --extra_summary_arguments Supply the non-default options for the ariba summary command.
        Wrap these in quotes e.g '--preset minimal --min_id 95'
   """.stripIndent()
}

//  print help if required
params.help = false
// Show help message
if (params.help){
    versionMessage()
    helpMessage()
    exit 0
}

// Show version number
params.version = false
if (params.version){
    versionMessage()
    exit 0
}

/***************** Setup inputs and channels ************************/
// Defaults for configurable variables
params.input_dir = false
params.fastq_pattern = false
params.bam_pattern = false
params.output_dir = false
params.accession_number_file = false
params.database_dir = false
params.get_database = false
params.extra_summary_arguments = false


// check if getting data either locally or from SRA
Helper.check_optional_parameters(params, ['input_dir', 'accession_number_file'])

// set up output directory
output_dir = file(Helper.check_mandatory_parameter(params, "output_dir"))
// set up ariba database
Helper.check_optional_parameters(params, ['database_dir', 'get_database'])

// make database if required
if (params.get_database) {
  database_name = params.get_database
  process get_database{
    tag { 'get database'}
    input:
    database_name

    output:
    file("${database_name}.out") into database_dir

    script:
    """
    ariba getref ${database_name} ${database_name}
    ariba prepareref -f ${database_name}.fa -m ${database_name}.tsv ${database_name}.out
    """
  }
} else if (params.database_dir){
  database_dir = file(params.database_dir)
}

// get data
if (params.accession_number_file){
  accession_number_file = params.accession_number_file - ~/\/$/
  // Fetch samples from ENA
  Channel
      .fromPath(accession_number_file)
      .splitText()
      .map{ x -> x.trim()}
      .set { accession_numbers }

  process fetch_from_ena {
    tag { accession_number }
    
    publishDir "${output_dir}/fastqs",
      mode: 'copy',
      saveAs: { file -> file.split('\\/')[-1] }

    input:
    val accession_number from accession_numbers

    output:
    set accession_number, file("${accession_number}/*.fastq.gz") into read_pairs

    """
    enaDataGet -f fastq -as /home/bio/.aspera/aspera.ini ${accession_number}
    """
  }
 
} else if (params.input_dir) {
  //  check a pattern has been specified
  Helper.check_optional_parameters(params, ['bam_pattern', 'fastq_pattern'])
  // set up read_pair channel if fastqs specified
  if ( params.fastq_pattern) {
      /*
      * Creates the `read_pairs` channel that emits for each read-pair a tuple containing
      * three elements: the pair ID, the first read-pair file and the second read-pair file
      */
      fastqs = params.input_dir + '/' + params.fastq_pattern
      Channel
        .fromFilePairs( fastqs )
        .ifEmpty { error "Cannot find any reads matching: ${fastqs}" }
        .set { read_pairs }

  } else if (params.bam_pattern) { // create read_pairs channel via bedtools if bams specified
    bams = params.input_dir + '/' + params.bam_pattern
    Channel
      .fromPath( bams )
      .ifEmpty { error "Cannot find any bam files matching: ${bams}" }
      .set {bam_files}

    process bam_to_paired_fastqs {
      tag {id}

      input:
      file(bam_file) from bam_files

      output:
      set val(id), file("*.fastq") into read_pairs

      script:
      id = bam_file.baseName
      """
      bedtools bamtofastq -i ${bam_file} -fq ${suffix}.R1.fastq -fq2 ${suffix}.R2.fastq
      """
    }
  }
}

 process run_ariba {
   tag {id}
   publishDir "${output_dir}", mode: 'copy'

   input:
   set id, file(file_pair)  from read_pairs
   file(database_dir)

   output:
   set id, file("${id}.ariba")
   file "${id}.report.tsv" into summary_channel

   """
   ariba run ${database_dir} ${file_pair[0]} ${file_pair[1]} ${id}.ariba
   cp ${id}.ariba/report.tsv ${id}.report.tsv
   """
 }

if (params.extra_summary_arguments){
  extra_summary_arguments = params.extra_summary_arguments
} else {
  extra_summary_arguments = ""
}

process run_ariba_summary {
  tag {'ariba summary'}
  publishDir "${output_dir}/summary", mode: 'copy'

  input:
  file report_tsvs from summary_channel.collect()
  file database_dir

  output:
  file "ariba_${database_base_name}_summary.*"

  script:
  database_base_name = database_dir.baseName
  """
  ariba summary ${extra_summary_arguments} ariba_${database_base_name}_summary ${report_tsvs}
  """



}

workflow.onComplete {
  Helper.complete_message(params, workflow, version)
}

workflow.onError {
  Helper.error_message(workflow)
}
