# Ariba workflow
This [Nextflow](https://www.nextflow.io/) workflow is designed to process short read data in the form of pairs of fastq files to predict AMR determinants or virulence factors using the [Ariba](https://github.com/sanger-pathogens/ariba) software. 

Typically the workflow should be run as follows

```
nextflow run ariba.nf -with-docker bioinformant/ghru-ariba:1.0 -resume [options]
```

To run the test sets either of the following commands will work
 - Using both local fastqs and a local database as input

    ```
    nextflow run ariba.nf --input_dir test_input --fastq_pattern '*_{1,2}.fastq.gz' --output_dir test_output --database_dir local_ariba_db -with-docker bioinformant/ghru-ariba:1.0 -resume
    ```
  - Fetching fastqs using a list of accession numbers and fetching the latest resfinder database 
    ```
    nextflow run ariba.nf --accession_number_file accessions.txt --output_dir test_output --get_database resfinder  -with-docker bioinformant/ghru-ariba:1.0 -resume
    ```

The mandatory options that should be supplied are
  - A source of the fastq or bam files specified as either of the following
    - local files on disk using the `--input_dir` argument and either of the `--fastq_pattern` and `--bam_pattern` arguments
    - from a list of short read archive ERR or SRR accession numbers contained within a file specified by the `--accession_number_file` argument
  - The output from the pipeline will be written to the directory specified by the `--output_dir` argument
  - The database of AMR factors specified as either
    - a file path to a local database `--database_dir`
    - or using `--get_database` in which case the latest database will be downloaded and used. Valid values for this paramater include argannot, card, megares, plasmidfinder, resfinder, srst2_argannot, vfdb_core, vfdb_full, virulencefinder

The optional arguments are
 - `--extra_summary_arguments` Supply the non-default options for the ariba summary command. See [here](https://github.com/sanger-pathogens/ariba/wiki/Task%3A-summary) for details. These should be supplied within quotes e.g `--extra_summary_arguments '--preset minimal --min_id 95'`

## Workflow process
The workflow consists of the following steps

1. (Optional) get fastq file pairs from the ENA based on the ERR/SRR accessions supplied in the accession_number_file
2. If bam files are supplied convert these to fastq files using bedtools
3. (Optional) download database specified by the `--get_database` argument 
4. Run ariba using each fastq file pair as an input with the database specified.
5. Summarise all ariba outputs in a single file called abricate_<DATABASE>summary.tsv where <DATABASE> is that specified in the command

## Workflow outputs
These will be found in the directory specified by the `--output_dir` argument

  - (Optional) If accession numbers were used as the input source a directory called `fastqs` will contain the fastq file pairs for each accession number
  - An output directory `<SAMPLE NAME>.ariba` contain the ariba outputs
  - The ariba report for each sample labelled `<SAMPLE NAME>.report.tsv`
  - A summary directory containing the summary command outputs including
    - A summary of the loci found as a csv file
    - The tre and csv file needed to visualize the output in [Phandango](https://jameshadfield.github.io/phandango)
    ![phandango output](phandango_output.png)

## Software used within the workflow
  - [Ariba](https://github.com/sanger-pathogens/ariba) Antimicrobial Resistance Identification By Assembly
  