FROM ubuntu:16.04
# File Author / Maintainer
MAINTAINER Anthony Underwood

# Update the repository sources list
RUN apt-get update
RUN apt-get -y install software-properties-common python-software-properties
RUN add-apt-repository ppa:openjdk-r/ppa
RUN apt-get update
# install java
RUN apt-get -y install openjdk-7-jre
# install other files necessary for running commands
RUN apt-get -y install zip wget libfindbin-libs-perl git
# get and fastqc
RUN wget http://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v0.11.5.zip
RUN unzip fastqc_v0.11.5.zip
RUN chmod 755 FastQC/fastqc
RUN mv FastQC /usr/local/
RUN ln -s /usr/local/FastQC/fastqc /usr/local/bin/fastqc
# install trimmomatic
RUN wget http://www.usadellab.org/cms/uploads/supplementary/Trimmomatic/Trimmomatic-0.36.zip
RUN unzip Trimmomatic-0.36.zip
RUN mv Trimmomatic-0.36 /usr/local/trimmomatic
# install python
RUN apt-get -y install python python-pip
RUN pip install scipy numpy

# install make tools and then download and compile bwa
RUN apt-get -y install make gcc zlib1g-dev libncurses5-dev libncursesw5-dev libbz2-dev liblzma-dev
RUN wget https://downloads.sourceforge.net/project/bio-bwa/bwa-0.7.15.tar.bz2
RUN tar xvfj bwa-0.7.15.tar.bz2
RUN cd bwa-0.7.15; make
RUN mv  bwa-0.7.15/bwa /usr/local/bin/

# Make GATK available
COPY GenomeAnalysisTK.jar /usr/local/bin/
ENV GATK_JAR=/usr/local/bin/GenomeAnalysisTK.jar

# download Picard
RUN wget https://github.com/broadinstitute/picard/releases/download/1.141/picard-tools-1.141.zip
RUN unzip picard-tools-1.141.zip
RUN mv picard-tools-1.141/picard.jar /usr/local/bin/
ENV PICARD_JAR=/usr/local/bin/picard.jar

# Download PHEnix Python dependencies
# python libraries
RUN pip install bintrees==2.0.2 biopython==1.65 coverage==4.0.3 nose==1.3.7 PyVCF==0.6.8 PyYAML==3.11 Sphinx==1.3.5 sphinx-argparse==0.1.15 sphinx-rtd-theme==0.1.9 numpydoc==0.6.0
# samtools
RUN wget https://github.com/samtools/samtools/releases/download/1.4/samtools-1.4.tar.bz2
RUN tar xvfj samtools-1.4.tar.bz2
RUN cd samtools-1.4; make; make prefix=/usr/local install
# bcf tools
RUN wget https://github.com/samtools/bcftools/releases/download/1.4/bcftools-1.4.tar.bz2
RUN tar xvfj bcftools-1.4.tar.bz2
RUN cd bcftools-1.4; make; make prefix=/usr/local install


# Install PHEnix
RUN git clone https://github.com/phe-bioinformatics/PHEnix.git
RUN pip install -e PHEnix

# Clean up APT when done.
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
