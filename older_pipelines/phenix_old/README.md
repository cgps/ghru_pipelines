## Building docker image
```
docker build -t bioinformant/phenix -f phenix.Dockerfile .
docker login
docker push bioinformant/phenix
```

## Running the pipeline
Example of running the nextflow pipeline

```
 nextflow run phenix.nf -with-docker bioinformant/phenix --read_dir=legionella_data --reference=legionella_data/philadelphia.fasta --snp_pipeline_config=legionella_data/snp_pipeline_config.yml --output_dir=legionella_data_output --fastq_pattern_match='*_{1,2}.fastq.gz' -with-trace -with-timeline -with-dag flowchart.png -with-report -resume
 ```